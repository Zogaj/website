import random


class Controller:


    @classmethod

    def process_request(cls,req,gs):

       gs.my_choice = req.form.to_dict()['group1']

       gs.ai_choice=random.choice(['rock','paper','scissors'])

       cls.compare(gs)

    @classmethod
    def compare(self,gs):
        x = {"rock":"scissors", "scissors":"paper", "paper":"rock"}
        if x[gs.my_choice] == gs.ai_choice:
            gs.my_score += 1
            gs.result = "You won"

        if x[gs.ai_choice] == gs.my_choice:
            gs.ai_score += 1
            gs.result = "Computer won"

        else:
            gs.result= "Tie"
            pass


class GameState:
    def __init__(self):

        self.my_score=0
        self.ai_score=0
        self.my_choice=""
        self.ai_choice=""
        self.result=""
