
# A very simple Flask Hello World app for you to get started with...

from flask import Flask, render_template, request

from controller import Controller, GameState


app = Flask(__name__)

gs = GameState()

@app.route('/', methods=["GET", "POST"])

def index1():

    if request.method=="POST":
        Controller.process_request(request, gs)
        return render_template('index1.html', STATE=gs)

    return render_template('index1.html', STATE=gs)



